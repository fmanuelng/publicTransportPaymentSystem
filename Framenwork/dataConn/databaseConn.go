package dataConn

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Framenwork/Repository"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Framenwork/controllers/userHistories"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Framenwork/controllers/users"
)

var dB *gorm.DB

func init() {
	dir, er := filepath.Abs(filepath.Dir(os.Args[0]))
	if er != nil {
		log.Fatal(er)
	}
	if _, err := os.Stat(filepath.Join(dir, "defaults", "dbConn")); os.IsNotExist(err) {
		os.MkdirAll(filepath.Join(dir, "defaults", "dbConn"), os.ModePerm)
		file, er := os.Create(filepath.Join(dir, "defaults", "dbConn", "dbconn.json"))
		if er != nil {
			panic(er)
		}
		json.NewEncoder(file).Encode(json.RawMessage(`
			{
				"name":"postgres",
				"connString":"postgres://yxfmjnsb:DjzMdyvBpt0cTYpcfrm1FrlDAqkBUmCv@baasu.db.elephantsql.com:5432/yxfmjnsb"
			}
			`))
	}
	if dbConnfPath, ok := os.LookupEnv("paymentDbConn"); !ok {
		if err := initDB(filepath.ToSlash(filepath.Join(dir, "defaults", "dbConn", "dbconn.json"))); err != nil {
			panic(err)
		}
	} else {
		if er := initDB(dbConnfPath); er != nil {
			panic(er)
		}
	}
}

//initDB inicializa la conexion a la DB en base desde un archivo de configuracion
func initDB(path string) (err error) {
	type DbConf struct {
		Name       string `json:"name"`
		ConnString string `json:"connString"`
	}
	valConf := func(d *DbConf) error {
		if d.Name == "" {
			return errors.New("Se necesita proverer el nombre de la base de dtoas que se esta utilizando")
		}

		if d.ConnString == "" {
			return errors.New("Se necesita proverer la direccion url a la cual la base de datos se coenctara")
		}
		return nil
	}
	file, er := os.Open(path)
	if er != nil {
		err = er
		return
	}
	var dconf DbConf
	er = json.NewDecoder(file).Decode(&dconf)
	if er != nil {
		err = er
		return
	}
	if er = valConf(&dconf); er == nil {
		if dconf.Name != "" && dconf.ConnString != "" {
			db, er := gorm.Open(dconf.Name, dconf.ConnString)
			if er != nil {
				err = er
				return
			}
			Data.InitTables(db)
			dB = db
		}
	} else {
		err = fmt.Errorf("se encontro el siguiente error al intenart procesar la data proveniente a la configuracion. Mensaje de error: %v", er)
		return
	}
	return
}

//DbContext contexto de la conexion actual de la base de datos y el modelo con el cual fue inicializado
type DbContext interface {
	//ModelName retorna el nombre del modelo referenciado en esta instancia
	ModelName() string
	//GetModel retorn la implementacion de la interfaz IRepository del modelo referenciado
	GetModel() Repository.IRepository
}

//dbContext estructura que implementa la interfaz DbContext
type dbContext struct {
	//modelName nombre del modelo referenciado
	modelName string
	//model interfaz IRepository del modelo referenciado
	model Repository.IRepository
}

func (d *dbContext) ModelName() string {
	return d.modelName
}

func (d *dbContext) GetModel() Repository.IRepository {
	return d.model
}

//Instance retorna una nueva instancia de DbContext en base al tipo de modelo recibido como parametro
func Instance(modelType ModelTypeEnum) (context DbContext) {
	switch modelType {
	case User:
		context = &dbContext{
			modelName: modelType.String(),
			model:     users.GetRepository(dB),
		}
		return
	case UserHistory:
		context = &dbContext{
			modelName: modelType.String(),
			model:     userHistories.GetRepository(dB),
		}
	}
	return
}
