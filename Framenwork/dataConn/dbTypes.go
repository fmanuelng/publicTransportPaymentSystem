package dataConn

//ModelTypeEnum tipo de enumerado para identificar los diferentes tipos de modelos de tablas que existen en el sistema
type ModelTypeEnum int

const (
	//User identificador para el modelo 'user'
	User ModelTypeEnum = 1 + iota
	//UserHistory identificador para el modelo 'userHistory'
	UserHistory
)

//String implementacion de la interfaz stringer
func (m *ModelTypeEnum) String() string {
	switch *m {
	case User:
		return "userModel"
	case UserHistory:
		return "userHistoryModel"
	default:
		return ""
	}
}

//ModelTypeDescriptor tipo de mapa que describe de forma humana el tipo de enumerado que se maneja en el momento
type ModelTypeDescriptor map[ModelTypeEnum]string

var Types = &ModelTypeDescriptor{
	User:        "Entidad 'Usuario'",
	UserHistory: "Entidad 'Historial de usuario'",
}

//Description retorna la descripcion del enumerado recibido como parametro
func (m ModelTypeDescriptor) Description(modelType ModelTypeEnum) string {
	return m[modelType]
}
