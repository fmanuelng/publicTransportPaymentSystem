package Repository

import (
	"github.com/satori/go.uuid"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/infraestructure"
)

//IRepository interfaz designada para la capa framenwork. Debe de ser implementada en cada controlador de modelo del sistema
type IRepository interface {
	//GetAll se encarga de hacer una seleciion o 'query' de la data contenida en el modelo recibido como parametro y buscarlo en la base de datos. de ser el parametro nulo entonces en su defecto solo traer toda la data contenida en la tabla de la base de datos
	GetAll(where interface{}) (dta *infraestructure.DataResult)
	//GetByID se emcarga de hacer una seleccion o 'query' del ID unico de un modelo para asi buscarlo en la tabla contenida en la base de datos
	GetByID(id uuid.UUID) (dta *infraestructure.DataResult)
	//Insert se encarga de la insercion de datos desde el modelo a la tabla proveniente de la base de datos
	Insert(entity interface{}) (dta *infraestructure.DataResult)
	//Update se encarga de la actualizacion de los datos desde el modelo a la tabla proveniente de la base de datos
	Update(entity interface{}) (dta *infraestructure.DataResult)
	//Delete se encarga de eliminar la data descrita en el modelo en la tabla contenida en la base de datos
	Delete(entity interface{}) (dta *infraestructure.DataResult)
}
