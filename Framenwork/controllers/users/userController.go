package users

import (
	"encoding/json"
	"fmt"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/user"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/baseEntity"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/infraestructure"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Framenwork/Repository"
)

//userController estructura que implementa la interfaz IRepository
type userController struct {
	//db conexion actual a la base de datos
	db *gorm.DB
	//users lista de usuarios usada para almanenar los resultados de cada consulta o 'query'
	users user.Users
}

func (u *userController) GetAll(where interface{}) (dta *infraestructure.DataResult) {
	if where != nil {
		if v, ok := where.(*user.User); ok {
			if res := u.db.Where(v).Find(&u.users); res.Error != nil {
				dta = infraestructure.GetErrorResult(res.Error)
				return
			} else {
				dta, _ = infraestructure.New(u.users.ToJson())
				return
			}
		}
	}
	if res := u.db.Find(&u.users); res.Error != nil {
		dta = infraestructure.GetErrorResult(res.Error)
		return
	} else {
		r, er := json.Marshal(&u.users)
		if er != nil {
			dta = infraestructure.GetErrorResult(er)
			return
		}
		dta = &infraestructure.DataResult{
			Success: true,
			Data:    json.RawMessage(r),
		}
		return
	}
}
func (u *userController) GetByID(id uuid.UUID) (dta *infraestructure.DataResult) {
	base := baseEntity.BaseEntity{RowID: id}
	if res := u.db.Find(&u.users, user.User{BaseEntity: base}); res.Error != nil {
		dta = infraestructure.GetErrorResult(res.Error)
		return
	} else {
		dta, _ = infraestructure.New(u.users.ToJson())
		return
	}
	return
}
func (u *userController) Insert(entity interface{}) (dta *infraestructure.DataResult) {
	if v, ok := entity.(*user.User); ok {
		var usr user.User
		if res := u.db.FirstOrCreate(&usr, v); res.Error != nil {
			dta = infraestructure.GetErrorResult(res.Error)
			return
		}
		u.users = append(u.users, usr)
		d, er := infraestructure.New(u.users.ToJson())
		if er != nil {
			dta = infraestructure.GetErrorResult(er)
			return
		}
		dta = d
		return
	}
	return
}
func (u *userController) Update(entity interface{}) (dta *infraestructure.DataResult) {
	if v, ok := entity.(*user.User); ok {
		var usuario user.User
		base := baseEntity.BaseEntity{RowID: v.RowID}
		if res := u.db.Select(&user.User{BaseEntity: base}).Find(&usuario); res.Error != nil {
			dta = infraestructure.GetErrorResult(res.Error)
			return
		}
		if res := u.db.Model(&usuario).Update(v); res.Error != nil {
			dta = infraestructure.GetErrorResult(res.Error)
			return
		} else {
			if res.RowsAffected > 0 {
				dta = &infraestructure.DataResult{
					Success: true,
					Data:    json.RawMessage(fmt.Sprintf(`{"isUpdated":true,"id":"%s"}`, usuario.RowID.String())),
				}
				return
			}
		}
	}
	return
}
func (u *userController) Delete(entity interface{}) (dta *infraestructure.DataResult) {
	if v, ok := entity.(*user.User); ok {
		if res := u.db.Delete(v); res.Error != nil {
			dta = infraestructure.GetErrorResult(res.Error)
			return
		}
		if u.db.Select(v).RecordNotFound() {
			dta, _ = infraestructure.New(json.RawMessage(fmt.Sprintf(`{"isDeleted":true,"id":"%s"}`, v.RowID.String())))
			return
		}
	}
	return
}

//GetRepository retorna la implementacion de la interfaz IRepository nueva
func GetRepository(DB *gorm.DB) Repository.IRepository {
	if !DB.HasTable(&user.User{}) {
		DB.CreateTable(&user.User{})
	}
	return &userController{
		db:    DB,
		users: user.Users{},
	}
}
