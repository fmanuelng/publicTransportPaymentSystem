package userHistories

import (
	"encoding/json"
	"fmt"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/userHistory"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/baseEntity"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/infraestructure"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Framenwork/Repository"
)

//userHistoryController estructura que implementa la interfaz IRepository
type userHistoryController struct {
	//db conexion actual a la base de datos
	db *gorm.DB
	//userHistories lista de usuarios usada para almanenar los resultados de cada consulta o 'query'
	userHistories userHistory.UserHistories
}

func (u *userHistoryController) GetAll(where interface{}) (dta *infraestructure.DataResult) {
	if where != nil {
		if v, ok := where.(*userHistory.UserHistory); ok {
			if res := u.db.Where(v).Find(&u.userHistories); res.Error != nil {
				dta = infraestructure.GetErrorResult(res.Error)
				return
			} else {
				dta, _ = infraestructure.New(u.userHistories.ToJson())
				return
			}
		}
	}
	if res := u.db.Find(&u.userHistories); res.Error != nil {
		dta = infraestructure.GetErrorResult(res.Error)
		return
	} else {
		dta, _ = infraestructure.New(u.userHistories.ToJson())
		return
	}
}
func (u *userHistoryController) GetByID(id uuid.UUID) (dta *infraestructure.DataResult) {
	base := baseEntity.BaseEntity{RowID: id}
	if res := u.db.Find(&u.userHistories, userHistory.UserHistory{BaseEntity: base}); res.Error != nil {
		dta = infraestructure.GetErrorResult(res.Error)
		return
	} else {
		dta, _ = infraestructure.New(u.userHistories.ToJson())
		return
	}
	return
}
func (u *userHistoryController) Insert(entity interface{}) (dta *infraestructure.DataResult) {
	if v, ok := entity.(*userHistory.UserHistory); ok {
		var usrH userHistory.UserHistory
		if res := u.db.FirstOrCreate(&usrH, v); res.Error != nil {
			dta = infraestructure.GetErrorResult(res.Error)
			return
		} else {
			u.userHistories = append(u.userHistories, usrH)
			dta, _ = infraestructure.New(u.userHistories.ToJson())
			return
		}
	}
	return
}
func (u *userHistoryController) Update(entity interface{}) (dta *infraestructure.DataResult) {
	if v, ok := entity.(*userHistory.UserHistory); ok {
		var historial userHistory.UserHistory
		base := baseEntity.BaseEntity{RowID: v.RowID}
		if res := u.db.Select(&userHistory.UserHistory{BaseEntity: base}).Find(&historial); res.Error != nil {
			dta = infraestructure.GetErrorResult(res.Error)
			return
		}
		if res := u.db.Model(&historial).Update(v); res.Error != nil {
			dta = infraestructure.GetErrorResult(res.Error)
			return
		} else {
			if res.RowsAffected > 0 {
				dta, _ = infraestructure.New(json.RawMessage(fmt.Sprintf(`{"isUpdated":true,"modifieds":%d}`, res.RowsAffected)))
				return
			}
		}
	}
	return
}
func (u *userHistoryController) Delete(entity interface{}) (dta *infraestructure.DataResult) {
	if v, ok := entity.(*userHistory.UserHistory); ok {
		if res := u.db.Delete(v); res.Error != nil {
			dta = infraestructure.GetErrorResult(res.Error)
			return
		}
		if u.db.Select(v).RecordNotFound() {
			dta, _ = infraestructure.New(json.RawMessage(fmt.Sprintf(`{"isDeleted":true,"HistoryID":"%s"}`, v.RowID.String())))
			return
		}
	}
	return
}

//GetRepository retorna la implementacion de la interfaz IRepository nueva
func GetRepository(DB *gorm.DB) Repository.IRepository {
	DB.Model(&userHistory.UserHistory{}).AddForeignKey("row_id", "users(row_id)", "CASCADE", "CASCADE")
	if !DB.HasTable(&userHistory.UserHistory{}) {
		DB.CreateTable(&userHistory.UserHistory{})
	}
	return &userHistoryController{
		db:            DB,
		userHistories: userHistory.UserHistories{},
	}
}
