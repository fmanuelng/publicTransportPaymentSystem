# iron/go:dev is the alpine image with the go tools added
FROM golang

ADD . /go/src/gitlab.com/fmanuelng/publicTransportPaymentSystem

WORKDIR /go/src/gitlab.com/fmanuelng/publicTransportPaymentSystem

RUN go get ./
RUN go build

EXPOSE 8080

ENTRYPOINT ["./publicTransportPaymentSystem"]