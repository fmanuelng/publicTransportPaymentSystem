package userHistories

import (
	"encoding/json"
	"reflect"

	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/user"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/userHistory"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/baseEntity"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Framenwork/dataConn"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Helper/errors"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/baseViewModel"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Service/baseService"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Service/infraestructure"

	userView "gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/viewModels/user"
	userHistoryView "gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/viewModels/userHistory"
)

//userHistoryService Estructura designada a manejar el servicio de la vista 'userHistory'
type userHistoryService struct {
	//dbContext instancia del objeto DbContext quien contiene tanto la conexion actual de la db como ademas el controlador del modelo a quien se le halla hecho referencia. en este caso contiene el modelo 'userHistory'
	dbContext dataConn.DbContext
}

func (u *userHistoryService) Insert(viewModel interface{}) *infraestructure.ServiceResult {
	if v, ok := viewModel.(*userHistoryView.UserHistoryViewModel); ok {
		Tuser := user.User{
			NfcID:      v.UserDetails.NfcID,
			Amount:     v.UserDetails.Amount,
			Enabled:    v.UserDetails.Enabled,
			Locked:     v.UserDetails.Locked,
			LocketDate: v.UserDetails.LocketDate,
			LastReload: v.UserDetails.LastReload,
		}
		TuserHistory := &userHistory.UserHistory{
			UserDetails: Tuser,
			Date:        v.Date,
			Amount:      v.Amount,
		}
		if result := u.dbContext.GetModel().Insert(TuserHistory); !result.Success {
			return infraestructure.GetErrorResult(&errors.ErrFailedToInsert{ModelName: u.dbContext.ModelName()})
		} else {
			var userHistoryResult userHistory.UserHistories
			json.Unmarshal(result.Data, &userHistoryResult)
			usrHistoryView := userHistoryView.UserHistoryViewModel{
				BaseViewModel: baseViewModel.BaseViewModel{RowID: userHistoryResult[0].RowID},
				UserDetails: userView.UserViewModel{
					NfcID:      userHistoryResult[0].UserDetails.NfcID,
					Amount:     userHistoryResult[0].UserDetails.Amount,
					Enabled:    userHistoryResult[0].UserDetails.Enabled,
					Locked:     userHistoryResult[0].UserDetails.Locked,
					LocketDate: userHistoryResult[0].UserDetails.LocketDate,
					LastReload: userHistoryResult[0].UserDetails.LastReload,
				},
				Date:   userHistoryResult[0].Date,
				Amount: userHistoryResult[0].Amount,
			}
			b, er := json.Marshal(&usrHistoryView)
			if er != nil {
				return infraestructure.GetErrorResult(&errors.ErrSerialization{"userHistory"})
			}
			sr, _ := infraestructure.New("UserHistory Inserted", json.RawMessage(b))
			return sr
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrValueNotValid{Value: "viewModel", Have: reflect.TypeOf(viewModel).String(), Whant: "userHistoryView"})
}
func (u *userHistoryService) Update(viewModel interface{}) *infraestructure.ServiceResult {
	if v, ok := viewModel.(*userHistoryView.UserHistoryViewModel); ok {
		TuserHistory := &userHistory.UserHistory{
			BaseEntity: baseEntity.BaseEntity{RowID: v.RowID},
			UserDetails: user.User{
				NfcID:      v.UserDetails.NfcID,
				Amount:     v.UserDetails.Amount,
				Enabled:    v.UserDetails.Enabled,
				Locked:     v.UserDetails.Locked,
				LocketDate: v.UserDetails.LocketDate,
				LastReload: v.UserDetails.LastReload,
			},
			Date:   v.Date,
			Amount: v.Amount,
		}
		if result := u.dbContext.GetModel().Update(TuserHistory); !result.Success {
			return infraestructure.GetErrorResult(&errors.ErrFailedToUpdate{"userHistory"})
		} else {
			sr, _ := infraestructure.New("UserHistory Update", result.Data)
			return sr
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrValueNotValid{Value: "viewModel", Have: reflect.TypeOf(viewModel).String(), Whant: "userHistoryView"})
}
func (u *userHistoryService) Delete(viewModel interface{}) *infraestructure.ServiceResult {
	if v, ok := viewModel.(*userHistoryView.UserHistoryViewModel); ok {
		TuserHistory := &userHistory.UserHistory{
			BaseEntity: baseEntity.BaseEntity{RowID: v.RowID},
			UserDetails: user.User{
				NfcID:      v.UserDetails.NfcID,
				Amount:     v.UserDetails.Amount,
				Enabled:    v.UserDetails.Enabled,
				Locked:     v.UserDetails.Locked,
				LocketDate: v.UserDetails.LocketDate,
				LastReload: v.UserDetails.LastReload,
			},
			Date:   v.Date,
			Amount: v.Amount,
		}
		if result := u.dbContext.GetModel().Delete(TuserHistory); !result.Success {
			return infraestructure.GetErrorResult(&errors.ErrFailedToDelete{"userHistory"})
		} else {
			sr, _ := infraestructure.New("UserHistory Deleted", result.Data)
			return sr
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrValueNotValid{Value: "viewModel", Have: reflect.TypeOf(viewModel).String(), Whant: "userHistoryView"})
}
func (u *userHistoryService) GetAll(viewModel interface{}) *infraestructure.ServiceResult {
	if viewModel != nil {
		if v, ok := viewModel.(*userHistoryView.UserHistoryViewModel); ok {
			TuserHistory := &userHistory.UserHistory{
				BaseEntity: baseEntity.BaseEntity{RowID: v.RowID},
				UserDetails: user.User{
					NfcID:      v.UserDetails.NfcID,
					Amount:     v.UserDetails.Amount,
					Enabled:    v.UserDetails.Enabled,
					Locked:     v.UserDetails.Locked,
					LocketDate: v.UserDetails.LocketDate,
					LastReload: v.UserDetails.LastReload,
				},
				Date:   v.Date,
				Amount: v.Amount,
			}
			if result := u.dbContext.GetModel().GetAll(TuserHistory); !result.Success {
				return infraestructure.GetErrorResult(&errors.ErrFailedToSelect{ModelName: "userHistory"})
			} else {
				sr, _ := infraestructure.New("UserHistory Selected", result.Data)
				return sr
			}
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrValueNotValid{Value: "viewModel", Have: reflect.TypeOf(viewModel).String(), Whant: "userHistoryView"})
}
func (u *userHistoryService) GetByID(id uuid.UUID) *infraestructure.ServiceResult {
	if ID, er := uuid.FromBytes(id.Bytes()); er == nil {
		if result := u.dbContext.GetModel().GetByID(ID); !result.Success {
			return infraestructure.GetErrorResult(&errors.ErrFailedToSelectByID{"userHistory"})
		} else {
			var userHistoryResult userHistory.UserHistories
			json.Unmarshal(result.Data, &userHistoryResult)
			usrHistoryView := userHistoryView.UserHistoryViewModel{
				BaseViewModel: baseViewModel.BaseViewModel{RowID: userHistoryResult[0].RowID},
				UserDetails: userView.UserViewModel{
					NfcID:      userHistoryResult[0].UserDetails.NfcID,
					Amount:     userHistoryResult[0].UserDetails.Amount,
					Enabled:    userHistoryResult[0].UserDetails.Enabled,
					Locked:     userHistoryResult[0].UserDetails.Locked,
					LocketDate: userHistoryResult[0].UserDetails.LocketDate,
					LastReload: userHistoryResult[0].UserDetails.LastReload,
				},
				Date:   userHistoryResult[0].Date,
				Amount: userHistoryResult[0].Amount,
			}
			b, er := json.Marshal(&usrHistoryView)
			if er != nil {
				return infraestructure.GetErrorResult(&errors.ErrSerialization{"userHistory"})
			}
			sr, _ := infraestructure.New("UserHistory Found", json.RawMessage(b))
			return sr
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrFailedToSelectByID{"userHistory"})
}

//GetService Retorna una instancia de userHistoryService quien hace la implementacion de la interfaz IBaseService
func GetService() baseService.IBaseService {
	return &userHistoryService{
		dbContext: dataConn.Instance(dataConn.UserHistory),
	}
}
