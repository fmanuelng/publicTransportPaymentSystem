package users

import (
	"encoding/json"
	"reflect"

	uuid "github.com/satori/go.uuid"

	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/user"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Framenwork/dataConn"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Helper/errors"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/baseViewModel"
	userView "gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/viewModels/user"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Service/baseService"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Service/infraestructure"
)

//userService Estructura designada a manejar el servicio de la vista 'user'
type userService struct {
	//dbContext instancia del objeto DbContext quien contiene tanto la conexion actual de la db como ademas el controlador del modelo a quien se le halla hecho referencia. en este caso contiene el modelo 'user'
	dbContext dataConn.DbContext
}

func (u *userService) Insert(viewModel interface{}) *infraestructure.ServiceResult {
	if v, ok := viewModel.(*userView.UserViewModel); ok {
		Tuser := &user.User{
			NfcID:      v.NfcID,
			Amount:     v.Amount,
			Enabled:    v.Enabled,
			Locked:     v.Locked,
			LocketDate: v.LocketDate,
			LastReload: v.LastReload,
		}
		if result := u.dbContext.GetModel().Insert(Tuser); !result.Success {
			return infraestructure.GetErrorResult(&errors.ErrFailedToInsert{ModelName: u.dbContext.ModelName()})
		} else {
			var userResult user.Users
			json.Unmarshal(result.Data, &userResult)
			usrView := userView.UserViewModel{
				BaseViewModel: baseViewModel.BaseViewModel{RowID: userResult[0].RowID},
				NfcID:         userResult[0].NfcID,
				Amount:        userResult[0].Amount,
				Enabled:       userResult[0].Enabled,
				Locked:        userResult[0].Locked,
				LocketDate:    userResult[0].LocketDate,
				LastReload:    userResult[0].LastReload,
			}
			b, er := json.Marshal(&usrView)
			if er != nil {
				return infraestructure.GetErrorResult(&errors.ErrSerialization{"user"})
			}
			sr, _ := infraestructure.New("User Inserted", json.RawMessage(b))
			return sr
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrValueNotValid{Value: "viewModel", Have: reflect.TypeOf(viewModel).String(), Whant: "userView"})
}
func (u *userService) Update(viewModel interface{}) *infraestructure.ServiceResult {
	if v, ok := viewModel.(*userView.UserViewModel); ok {
		Tuser := &user.User{
			NfcID:      v.NfcID,
			Amount:     v.Amount,
			Enabled:    v.Enabled,
			Locked:     v.Locked,
			LocketDate: v.LocketDate,
			LastReload: v.LastReload,
		}
		if result := u.dbContext.GetModel().Update(Tuser); !result.Success {
			return infraestructure.GetErrorResult(&errors.ErrFailedToUpdate{"user"})
		} else {
			sr, _ := infraestructure.New("User Updated", result.Data)
			return sr
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrValueNotValid{Value: "viewModel", Have: reflect.TypeOf(viewModel).String(), Whant: "userView"})
}
func (u *userService) Delete(viewModel interface{}) *infraestructure.ServiceResult {
	if v, ok := viewModel.(*userView.UserViewModel); ok {
		Tuser := &user.User{
			NfcID:      v.NfcID,
			Amount:     v.Amount,
			Enabled:    v.Enabled,
			Locked:     v.Locked,
			LocketDate: v.LocketDate,
			LastReload: v.LastReload,
		}
		if result := u.dbContext.GetModel().Delete(&Tuser); !result.Success {
			return infraestructure.GetErrorResult(&errors.ErrFailedToDelete{"user"})
		} else {
			sr, _ := infraestructure.New("User Deleted", result.Data)
			return sr
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrValueNotValid{Value: "viewModel", Have: reflect.TypeOf(viewModel).String(), Whant: "userView"})
}
func (u *userService) GetAll(viewModel interface{}) *infraestructure.ServiceResult {
	if viewModel != nil {
		if v, ok := viewModel.(*userView.UserViewModel); ok {
			Tuser := &user.User{
				NfcID:      v.NfcID,
				Amount:     v.Amount,
				Enabled:    v.Enabled,
				Locked:     v.Locked,
				LocketDate: v.LocketDate,
				LastReload: v.LastReload,
			}
			if result := u.dbContext.GetModel().GetAll(Tuser); !result.Success {
				return infraestructure.GetErrorResult(&errors.ErrFailedToSelect{ModelName: "user"})
			} else {
				sr, _ := infraestructure.New("User Selected", result.Data)
				return sr
			}
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrValueNotValid{Value: "viewModel", Have: reflect.TypeOf(viewModel).String(), Whant: "userView"})
}
func (u *userService) GetByID(id uuid.UUID) *infraestructure.ServiceResult {
	if ID, er := uuid.FromBytes(id.Bytes()); er == nil {
		if result := u.dbContext.GetModel().GetByID(ID); !result.Success {
			return infraestructure.GetErrorResult(&errors.ErrFailedToSelectByID{ModelName: "user"})
		} else {
			var userResult user.Users
			json.Unmarshal(result.Data, &userResult)
			usrView := userView.UserViewModel{
				BaseViewModel: baseViewModel.BaseViewModel{RowID: userResult[0].RowID},
				NfcID:         userResult[0].NfcID,
				Amount:        userResult[0].Amount,
				Enabled:       userResult[0].Enabled,
				Locked:        userResult[0].Locked,
				LocketDate:    userResult[0].LocketDate,
				LastReload:    userResult[0].LastReload,
			}
			b, er := json.Marshal(&usrView)
			if er != nil {
				return infraestructure.GetErrorResult(&errors.ErrSerialization{"user"})
			}
			sr, _ := infraestructure.New("User Found", json.RawMessage(b))
			return sr
		}
	}
	return infraestructure.GetErrorResult(&errors.ErrFailedToSelectByID{ModelName: "user"})
}

//GetService Retorna una instancia de userService quien hace la implementacion de la interfaz IBaseService
func GetService() baseService.IBaseService {

	return &userService{
		dbContext: dataConn.Instance(dataConn.User),
	}
}
