package baseService

import (
	"github.com/satori/go.uuid"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Service/infraestructure"
)

//IBaseService Interfaz designada para la capa servicio. En la misma se hace la implementacion de las vistas de cada modelo del sistema
type IBaseService interface {
	//Insert se encarga de la insercion de datos desde la vista al modelo proveniente de la base de datos
	Insert(viewModel interface{}) *infraestructure.ServiceResult
	//Update se encarga de la actualizacion de los datos desde la vista al modelo proveniente de la base de datos
	Update(viewModel interface{}) *infraestructure.ServiceResult
	//Delete se encarga de eliminar la data descrita en la vista el en modelo de la base de datos
	Delete(viewModel interface{}) *infraestructure.ServiceResult
	//GetAll se encarga de hacer una seleciion o 'query' de la data contenida en la vista y buscarla en el modelo almacenado en la base de datos o en su defecto solo traer toda la data contenida en el modelo
	GetAll(viewModel interface{}) *infraestructure.ServiceResult
	//GetByID se emcarga de hacer una seleccion o 'query' del ID unico de una vista para asi buscarla en el modelo de base de datos
	GetByID(id uuid.UUID) *infraestructure.ServiceResult
}
