package infraestructure

import (
	"encoding/json"
	"errors"
	"fmt"
)

//ServiceResult es el objeto usado para la respuesta del aplicativo como un servicio
type ServiceResult struct {
	Success     bool
	ResultData  json.RawMessage
	ResultTitle string
}

//GetErrorResult retorna un objeto ServiceResult para manejar el error que se haya recibido en el momento
func GetErrorResult(err error) *ServiceResult {
	return &ServiceResult{
		Success:     false,
		ResultTitle: "Error",
		ResultData:  json.RawMessage(fmt.Sprintf(`{"error":"%s"}`, err.Error())),
	}
}

//New retorna un nuevo objeto ServiceResult en base a el titulo o accion que se halla hecho (como seria 'Model Updated') y la data en formato crudo del servicio a quien se le halla implementado
func New(title string, data json.RawMessage) (sr *ServiceResult, err error) {
	if title != "" && data != nil {
		sr = &ServiceResult{
			Success:     true,
			ResultTitle: title,
			ResultData:  data,
		}
		return
	}
	err = errors.New("the fields 'title' and 'data' can not be empty !")
	return
}
