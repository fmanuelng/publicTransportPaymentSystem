package userHistoryRouter

import (
	"encoding/json"
	"net/http"

	"github.com/labstack/echo"
	uuid "github.com/satori/go.uuid"
	userHistoryView "gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/viewModels/userHistory"
	userHistoryService "gitlab.com/fmanuelng/publicTransportPaymentSystem/Service/implementations/userHistories"
)

//getAll obtiene todos los historiales de usuario almacenados en el sistema
func getAll(c echo.Context) error {
	return c.JSON(http.StatusOK, userHistoryService.GetService().GetAll(nil))
}

//postGetAllWhere obtiene los historiales que cumplan con la vista recibida como parametro
func postGetAllWhere(c echo.Context) error {
	var userHistoryParam userHistoryView.UserHistoryViewModel
	if err := json.NewDecoder(c.Request().Body).Decode(&userHistoryParam); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, userHistoryService.GetService().GetAll(&userHistoryParam))
}

//getByID retorna el historial de usuario que sea protador del id unico recibido como parametro
func getByID(c echo.Context) error {
	if id := c.Param("id"); id != "" {
		ID, er := uuid.FromString(id)
		if er != nil {
			c.Error(er)
		}
		return c.JSON(http.StatusOK, userHistoryService.GetService().GetByID(ID))
	}
	return echo.NewHTTPError(http.StatusBadRequest, "the id of the user history is necesary !")
}

//postNew crea un nuevo historial de usuario en base a la vista resibida en el request
func postNew(c echo.Context) error {
	var userHistoryParam userHistoryView.UserHistoryViewModel

	if err := json.NewDecoder(c.Request().Body).Decode(&userHistoryParam); err != nil {
		return err
	}

	return c.JSON(http.StatusOK, userHistoryService.GetService().Insert(&userHistoryParam))

}

//postUpdate modifica el historail de usuario que cumpla con la vista recibida como parametro y modifica el modelo en base a las diferencias de informacion encontradas en la vista
func postUpdate(c echo.Context) error {
	var userHistoryParam userHistoryView.UserHistoryViewModel
	if err := json.NewDecoder(c.Request().Body).Decode(&userHistoryParam); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, userHistoryService.GetService().Update(&userHistoryParam))
}

//postDelete elimina el historial de usuario en base a la vista resibida en el request
func postDelete(c echo.Context) error {
	var userHistoryParam userHistoryView.UserHistoryViewModel
	if err := json.NewDecoder(c.Request().Body).Decode(&userHistoryParam); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, userHistoryService.GetService().Delete(&userHistoryParam))
}

//InitRoute inicializa las rutas del controlador de 'historial de usuario'
func InitRoute(e *echo.Echo) {
	PathPrefix := "/api/userHistories"
	g := e.Group(PathPrefix)
	g.GET("/getAll", getAll)
	g.POST("/getAllWhere", postGetAllWhere)
	g.GET("/ByID/:id", getByID)
	g.POST("/new", postNew)
	g.POST("/update", postUpdate)
	g.POST("/delete", postDelete)
}
