package Routes

import (
	"github.com/labstack/echo"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Routes/arduinoRouter"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Routes/userHistoryRouter"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Routes/userRouter"
)

//InitRoutes Inicializa las rutas que tiene descritas el aplicativo en cuestion
func InitRoutes(e *echo.Echo) {
	userRouter.InitRoute(e)
	userHistoryRouter.InitRoute(e)
	arduinoRouter.InitRoute(e)
}
