package userRouter

import (
	"encoding/json"
	"net/http"

	"github.com/satori/go.uuid"

	"github.com/labstack/echo"
	userView "gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/viewModels/user"
	userService "gitlab.com/fmanuelng/publicTransportPaymentSystem/Service/implementations/users"
)

//getAll Obtener todos los usuarios contenidos en la vista 'user'
func getAll(c echo.Context) error {
	return c.JSON(http.StatusOK, userService.GetService().GetAll(nil))
}

//postGetAllWhere obtiene todos los usuarios que complan con la vista recibida en el request
func postGetAllWhere(c echo.Context) error {
	var userParam userView.UserViewModel
	if err := json.NewDecoder(c.Request().Body).Decode(&userParam); err != nil {
		c.Error(err)
	}
	return c.JSON(http.StatusOK, userService.GetService().GetAll(&userParam))
}

//getByID obtiene el usuario que sea propietario del id recibido como parametro
func getByID(c echo.Context) error {
	if id := c.Param("id"); id != "" {
		ID, er := uuid.FromString(id)
		if er != nil {
			c.Error(er)
		}
		return c.JSON(http.StatusOK, userService.GetService().GetByID(ID))
	}
	return echo.NewHTTPError(http.StatusBadRequest, "the id of the user is necesary !")
}

//postNew crea un nuevo usuario en base a la vista recibida en el request
func postNew(c echo.Context) error {
	var userParam userView.UserViewModel
	body := c.Request().Body
	if err := json.NewDecoder(body).Decode(&userParam); err != nil {
		c.Error(err)
	}

	return c.JSON(http.StatusOK, userService.GetService().Insert(&userParam))

}

//postUpdate modifica el usuario que se haya recibido en el request en base a la data que resulte distinta a la almacenada en el sistema
func postUpdate(c echo.Context) error {
	var userParam userView.UserViewModel
	if err := json.NewDecoder(c.Request().Body).Decode(&userParam); err != nil {
		c.Error(err)
	}
	return c.JSON(http.StatusOK, userService.GetService().Update(&userParam))
}

//postDelete elimina el usuario que se haya recibido en el request
func postDelete(c echo.Context) error {
	var userParam userView.UserViewModel
	if err := json.NewDecoder(c.Request().Body).Decode(&userParam); err != nil {
		c.Error(err)
	}
	return c.JSON(http.StatusOK, userService.GetService().Delete(&userParam))
}

//InitRoute Inicializa las rutas para el controlador 'user'
func InitRoute(e *echo.Echo) {
	PathPrefix := "/api/users"
	g := e.Group(PathPrefix)
	g.GET("/getAll", getAll)
	g.POST("/getAllWhere", postGetAllWhere)
	g.GET("/ByID/:id", getByID)
	g.POST("/new", postNew)
	g.POST("/update", postUpdate)
	g.POST("/delete", postDelete)
}
