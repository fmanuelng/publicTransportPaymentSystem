package arduinoRouter

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Helper/errors"
	userView "gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/viewModels/user"
	userHistoryView "gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/viewModels/userHistory"
	userHistoryService "gitlab.com/fmanuelng/publicTransportPaymentSystem/Service/implementations/userHistories"
	userService "gitlab.com/fmanuelng/publicTransportPaymentSystem/Service/implementations/users"
)

//ResponseData estructura manejada por el arduino para almacenar o manejar la data contenida en una tarjeta NFC
type ResponseData struct {
	//NfcID id unico de la tarjeta NFC
	NfcID string `json:"id"`
	//Amount monto actual que esta tiene guardado hasta el momento
	Amount float64 `json:"amount"`
}

//ErrorData estructura de error usada para deserializar los resultados de error del sistema o para generar los propios
type ErrorData struct {
	//Message mensaje de error
	Message string `json:"error"`
}

//Error implementacion de la interfaz error
func (e *ErrorData) Error() string {
	return e.Message
}

//postSetAmount guarda el monto actual que tenga contenido el arduino en la base de datos del aplicativo
func postSetAmount(c echo.Context) error {
	respDta := new(ResponseData)
	if err := json.NewDecoder(c.Request().Body).Decode(respDta); err != nil {
		return err
	}

	if userSelect := userService.GetService().GetAll(&userView.UserViewModel{NfcID: respDta.NfcID}); userSelect.Success {
		userList, er := userView.ToUserList(userSelect.ResultData)
		if er != nil {
			c.Error(er)
		}
		if len(userList) == 0 {
			if userSelect := userService.GetService().Insert(&userView.UserViewModel{NfcID: respDta.NfcID, Amount: respDta.Amount, Enabled: true, Locked: false, LastReload: time.Now()}); userSelect.Success {
				userList, er := userView.ToUserList(userSelect.ResultData)
				if er != nil {
					return echo.NewHTTPError(http.StatusBadRequest, error(&errors.ErrFailedToInsert{ModelName: "User"}))
				}
				if userHistoryInsert := userHistoryService.GetService().Insert(&userHistoryView.UserHistoryViewModel{UserDetails: userList[0], Date: time.Now(), Amount: respDta.Amount}); userHistoryInsert.Success {
					return c.JSON(http.StatusOK, userHistoryInsert)
				} else {
					return c.JSON(http.StatusNotModified, userHistoryInsert)
				}
			}
		}
		userV := userList[0]
		if userInserted := userHistoryService.GetService().Insert(&userHistoryView.UserHistoryViewModel{UserDetails: userV, Date: time.Now(), Amount: respDta.Amount}); userInserted.Success {
			return c.JSON(http.StatusOK, userInserted)
		} else {
			return c.JSON(http.StatusBadRequest, userInserted.ResultData)
		}
	} else {
		if userSelect := userService.GetService().Insert(&userView.UserViewModel{NfcID: respDta.NfcID, Amount: respDta.Amount, Enabled: true, Locked: false, LastReload: time.Now()}); userSelect.Success {
			userList, er := userView.ToUserList(userSelect.ResultData)
			if er != nil {
				c.Error(er)
			}
			if userHistoryInsert := userHistoryService.GetService().Insert(&userHistoryView.UserHistoryViewModel{UserDetails: userList[0], Date: time.Now(), Amount: respDta.Amount}); userHistoryInsert.Success {
				return c.JSON(http.StatusOK, userHistoryInsert)
			} else {
				return c.JSON(http.StatusNotModified, userHistoryInsert)
			}
		} else {
			return c.JSON(http.StatusNotModified, userSelect)
		}
	}
	return echo.NewHTTPError(http.StatusNotFound, "The user of this id is't not found !")
}

//getBalance obtiene un ResponseData con el balance actual que se tenga en base al id unico recibido como parametro
func getBalance(c echo.Context) error {
	if id := c.Param("ncfId"); id != "" {
		if userFound := userService.GetService().GetAll(&userView.UserViewModel{NfcID: id}); userFound.Success {
			userList, er := userView.ToUserList(userFound.ResultData)
			if er != nil {
				c.Error(er)
			}
			userV := userList[0]
			respDta := new(ResponseData)
			respDta.NfcID = userV.NfcID
			respDta.Amount = userV.Amount
			return c.JSON(http.StatusOK, respDta)
		}

	}
	return echo.NewHTTPError(http.StatusNotFound, "id of nfc card not ingresed !")
}

//InitRoute inicializa las rutas del controlador de request para el arduino en cuestion que haga una consulta
func InitRoute(e *echo.Echo) {
	PathPrefix := "/api/arduino"
	g := e.Group(PathPrefix)
	g.POST("/setAmount", postSetAmount)
	g.GET("/getBalance/:nfcId", getBalance)
}
