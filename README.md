# Public Transport Payment System (PTPS)

Este no es mas que el API server de este comjunto de utilidades para manejo de pagos medinates tarjetas NFC

<!-- TOC -->

- [Public Transport Payment System (PTPS)](#public-transport-payment-system-ptps)
    - [Funcionalidades](#funcionalidades)
    - [Instalacion](#instalacion)
    - [Uso](#uso)
    - [Herramientas Utilizadas](#herramientas-utilizadas)

<!-- /TOC -->

## Funcionalidades

1. Posibilidad de ser usado en diferentes dispositivos
2. Capacidad de ser utilizada con diferentes bases de datos
3. Posee un controlador para un dispositivo arduino para ser usado como lector

## Instalacion
Para ser instalado primero debemos de cumplir con los siguientes requerimientos:

1. Tener instalado el compilador del lenguaje de programacion Golang, puede obtenerlo [aqui](https://golang.org/).
2. Tener instalado Git en su computador, para distribuciones Windows y Mac puede obtenerlo [aqui](https://git-scm.com/). Para distribuciones linux debera de buscar el como instalarlo para su sabor de distro.

Una vez haya cumplido con los requisitos pautados debera hacer lo siguiente:

1. Debe de abrir una consola o cmd y teclear el siguiente comando:
            `go get gitlab.com/fmanuelng/publicTransportPaymentSystem/...`
2. Luego debera de aceder a la ubicacion de su GOPATH (mas informacion [aqui](https://github.com/golang/go/wiki/SettingGOPATH), aunque ya por defecto golang al momento de ejecutar el comando go get genero una carpeta denominada *go* en su carpeta de usuario, en windows esta se ubica en /users/{miUsuaio}/go, y en sistemas mac y linux esta en ~/go) y seguir la ruta gitlab.com/fmanuelng/publicTransportPaymentSystem/
3. Una vez dentro de esta carpeta abra una consola dentro de esta y ejecute el siguiente comando: `go get ./` para asi obtener todas las dependencias que contiene el proyecto en si.
4. Una vez terminada toda la operacion de descargas de dependencias ejecute este comando para generar un ejecutable para su sistema operativo: `go build` y luego `go run`


## Uso
El sistema cuenta con diferentes rutas o controlers para los diferentes tipos de solicitudes web que se manejan y las cuales son: 
1. User (/api/users)
    1. /getAll: Obtiene todos los usuarios que se tengan almacenados en la base de datos.
    2. /getAllWhere: Obtiene los o el usuario que cumpla con la vista enviada como request al sistema
    3. /ByID/:id: Obtiene el usuario portador del id recibido como parametro.
    4. /new: Crea un nuevo usuario en base a la vista usuario recibida en el request.
    5. /update: Actualiza un usuario en base a la vista recibida como parametro y la cual debe de contener el id de usuario a moificar.
    6. /delete: Elimina el usuario que cumpla con el id contenido en la vista usario recibida como parametro.
2. UserHistory (/api/userHistories) 
    1. /getAll: Obtiene todos los historiales de usuario que se tengan almacenados en la base de datos.
    2. /getAllWhere: Obtiene los o el historial de usuario que cumpla con la vista enviada como request al sistema.
    3. /ByID/:id: Obtiene el historial de usuario portador del id recibido como parametro
    4. /new: Crea un nuevo historial de usuario en base a la vista historial de usuario recibida en el request.
    5. /update: Actualiza un historial de usuario en base a la vista recibida como parametro y la cual debe de contener el id del historial de usuario a moificar.
    6. /delete: Elimina el historial de usuario que cumpla con el id contenido en la vista historial de usario recibida como parametro.
3. Arduino (/api/arduino)
    1. /setAmount: Crea o actualiza un usuario y su historial de recargas en base al ID de su tarjeta NFC portada en base al ResponseData recibido de la interfaz [nfcUpdater](https://gitlab.com/fmanuelng/nfcUpdater).  
    2. /getBalance/:nfcId: Retorna un ResponseData en base al ID NFC registrado en la base de datos con el ultimo monto agregado a l mismo. De no ser encontrado este retornara un BadRequest (error 403).
## Herramientas Utilizadas

En este sistema fueron utilizados las siguientes herramientas:

[**Echo**](https://echo.labstack.com/): Framenwork Web  
[**GORM**](http://gorm.io/): ORM para bases de datos SQL  