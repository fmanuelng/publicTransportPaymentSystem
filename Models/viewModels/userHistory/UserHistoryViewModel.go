package userHistory

import (
	"encoding/json"
	"time"

	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/userHistory"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/baseViewModel"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/viewModels/user"
)

//UserHistoryViewModel estructura de la vista 'historial de usuario'
type UserHistoryViewModel struct {
	baseViewModel.BaseViewModel
	UserDetails user.UserViewModel `json:"userDetails" form:"userDetails"`
	Date        time.Time          `json:"date" form:"date"`
	Amount      float64            `json:"amount" form:"amount"`
}

//UsersHistoriesViewModel lista de vistas de 'historial de usuario'
type UsersHistoriesViewModel []UserHistoryViewModel

//ToUserHistoryList retorna una lista de historiales de usuario en base a la data cruda de json que se recibe como parametro. Si la data recibida resulta imposible de deserializar entonces esta retorna error
func ToUserHistoryList(raw json.RawMessage) (usrHistoryList UsersHistoriesViewModel, err error) {
	var usrHistoryListE userHistory.UserHistories
	if er := json.Unmarshal(raw, &usrHistoryListE); er != nil {
		err = er
		return
	}
	usersHistoriesListV := make(UsersHistoriesViewModel, len(usrHistoryListE))
	for pos, usrHistory := range usrHistoryListE {
		usrHistoryV := UserHistoryViewModel{
			BaseViewModel: baseViewModel.BaseViewModel{ID: usrHistory.ID, RowID: usrHistory.RowID},
			UserDetails: user.UserViewModel{
				BaseViewModel: baseViewModel.BaseViewModel{ID: usrHistory.UserDetails.ID, RowID: usrHistory.UserDetails.RowID},
				NfcID:         usrHistory.UserDetails.NfcID,
				Amount:        usrHistory.UserDetails.Amount,
				Enabled:       usrHistory.UserDetails.Enabled,
				Locked:        usrHistory.UserDetails.Locked,
				LocketDate:    usrHistory.UserDetails.LocketDate,
				LastReload:    usrHistory.UserDetails.LastReload,
			},
			Date:   usrHistory.Date,
			Amount: usrHistory.Amount,
		}
		usersHistoriesListV[pos] = usrHistoryV
	}
	usrHistoryList = usersHistoriesListV
	return
}
