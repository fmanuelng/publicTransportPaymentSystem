package user

import (
	"encoding/json"
	"time"

	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/user"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Models/baseViewModel"
)

//UserViewModel estructura de la vista 'user'
type UserViewModel struct {
	baseViewModel.BaseViewModel
	NfcID      string    `json:"nfcId" form:"nfcId" query:"nfcId"`
	Amount     float64   `json:"amount" form:"amount" query:"amount"`
	Enabled    bool      `json:"enabled" form:"enabled" query:"enabled"`
	Locked     bool      `json:"locked" form:"locked" query:"locked"`
	LocketDate time.Time `json:"lockedDate,omitempty" form:"lockedDate" query:"lockedDate"`
	LastReload time.Time `json:"lastReload" form:"lastReload" query:"lastReload"`
}

//UserListViewModel Lista de vistas de 'user'
type UserListViewModel []UserViewModel

//ToUserList retorna una lista de usuarios en base a la data cruda de json que se recibe como parametro. Si la data recibida resulta imposible de deserializar entonces esta retorna error
func ToUserList(raw json.RawMessage) (usrList UserListViewModel, err error) {
	var usersList user.Users
	if er := json.Unmarshal(raw, &usersList); er != nil {
		err = er
		return
	}
	usersListV := make(UserListViewModel, len(usersList))
	for pos, usr := range usersList {
		usrV := UserViewModel{
			BaseViewModel: baseViewModel.BaseViewModel{ID: usr.ID, RowID: usr.RowID},
			NfcID:         usr.NfcID,
			Amount:        usr.Amount,
			Enabled:       usr.Enabled,
			Locked:        usr.Locked,
			LocketDate:    usr.LocketDate,
			LastReload:    usr.LastReload,
		}
		usersListV[pos] = usrV
	}
	usrList = usersListV
	return
}
