package baseViewModel

import (
	"github.com/satori/go.uuid"
)

//BaseViewModel estructura por defecto para la capa Models. La misma debe de ser implementada en cada vista que vaya a contener el sistema en cuestion
type BaseViewModel struct {
	//ID id de tipo numerico utilizado de forma interna
	ID uint
	//RowID id unico de la vista en cuestion
	RowID uuid.UUID `json:"id"`
}
