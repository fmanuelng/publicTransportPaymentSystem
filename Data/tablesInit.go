package Data

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/user"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/userHistory"
)

//InitTables inicializa todas las tablas del sistema en la base de datos
func InitTables(DB *gorm.DB) {
	DB.AutoMigrate(&user.User{}, &userHistory.UserHistory{})
}
