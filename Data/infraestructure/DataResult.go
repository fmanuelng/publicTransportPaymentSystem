package infraestructure

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
)

type DataResult struct {
	Success bool
	Data    json.RawMessage
}

func New(data interface{}) (dta *DataResult, err error) {
	if b, ok := data.(json.RawMessage); ok {
		dta = &DataResult{
			Success: true,
			Data:    b,
		}
		return
	}
	if reflect.ValueOf(data).Kind() == reflect.Ptr && reflect.ValueOf(data).Kind() == reflect.Struct {
		r, er := json.Marshal(data)
		if er != nil {
			err = er
			return
		}
		dta = &DataResult{
			Success: true,
			Data:    json.RawMessage(r),
		}
		return
	}
	//TODO: agregar un error personalizado para cuando la data recibida no sea un puntero
	return nil, errors.New("The data received is't not a pointer")
}

func GetErrorResult(err error) (dta *DataResult) {
	dta = &DataResult{
		Success: false,
		Data:    json.RawMessage([]byte(fmt.Sprintf(`{"error":"%s"}`, err.Error()))),
	}
	return
}
