package user

import (
	"encoding/json"
	"time"

	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/baseEntity"
)

//User Entidad de Usuario
type User struct {
	baseEntity.BaseEntity
	NfcID      string    `json:"nfcId" gorm:"unique_index"`
	Amount     float64   `json:"amount"`
	Enabled    bool      `json:"enabled"`
	Locked     bool      `json:"locked"`
	LocketDate time.Time `json:"lockedDate"`
	LastReload time.Time `json:"lastReload"`
}

//Users Lista de entidades de usuarios
type Users []User

//ToJson Retorna un json.RawMessage (objeto json serializado) del listado de usuarios
func (u *Users) ToJson() json.RawMessage {
	b, _ := json.Marshal(u)
	return json.RawMessage(b)
}
