package userHistory

import (
	"encoding/json"
	"time"

	"github.com/satori/go.uuid"

	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/Entities/user"

	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Data/baseEntity"
)

//UserHistory Entidad de historial de usuario
type UserHistory struct {
	baseEntity.BaseEntity
	UserDetails user.User `json:"userDetails"`
	UserID      uuid.UUID `gorm:"foreignkey:row_id"`
	Date        time.Time `json:"date"`
	Amount      float64   `json:"amount"`
}

//UserHistories Lista de historiales de usuario
type UserHistories []UserHistory

func (u *UserHistories) ToJson() json.RawMessage {
	b, _ := json.Marshal(u)
	return json.RawMessage(b)
}
