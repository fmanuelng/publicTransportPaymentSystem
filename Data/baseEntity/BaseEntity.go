package baseEntity

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

//BaseEntity estructura por defecto para la capa Data. La misma debe de ser implementada en cada modelo que se vaya a utilizar en el sistema
type BaseEntity struct {
	//gorm.Model estructura por defecto utilizada por el ORM
	gorm.Model
	//RowID id unico del modelo
	RowID uuid.UUID `json:"id" sql:"type:uuid;primary_key;default:uuid_generate_v4()"`
}
