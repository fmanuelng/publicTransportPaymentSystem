package errors

import (
	"fmt"
)

//ErrValueNotValid se produce cuando no se ha recibido un valor no esperado para la funcion quien lo utiliza
type ErrValueNotValid struct {
	//Value valor manejado en el momento del error
	Value string
	//Have tipo de dato que almacena el valor manejado
	Have string
	//Whant valor esperado en el momento del error
	Whant string
}

//Error implementacion de la interfaz error
func (e *ErrValueNotValid) Error() string {
	return fmt.Sprintf("the value '%s' contains data not valid for this function, expected value '%s', value received '%s' ", e.Value, e.Whant, e.Have)
}
