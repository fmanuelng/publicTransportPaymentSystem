package errors

import (
	"fmt"
)

//ErrFailedToSelect error retornado cuando no se pudo hacer la operacion de seleccion o 'query' sobre el modelo el cual invoca esta operacion
type ErrFailedToSelect struct {
	//ModelName Nombre del modelo
	ModelName string
	//Reason razon del error
	Reason string
}

//Error implementacion de la interfaz error
func (e *ErrFailedToSelect) Error() string {
	if e.Reason != "" {
		return fmt.Sprintf("the model '%s' can not be selected !, reason: %s", e.ModelName, e.Reason)
	}
	return fmt.Sprintf("the model '%s' can not be selected !", e.ModelName)
}

//ErrFailedToSelectByID error retornado cuando no se pudo hacer la seleccion sobre el modelo referenciado en base a su id
type ErrFailedToSelectByID struct {
	ModelName string
}

//Error implementacion de la interfaz error
func (e *ErrFailedToSelectByID) Error() string {
	return fmt.Sprintf("the model '%s' can not be found by the received id value or the id is't not valid  !", e.ModelName)
}

//ErrFailedToInsert error retornado cuando no se pudo hacer la insercion del modelo de forma correcta en la base de datos
type ErrFailedToInsert struct {
	ModelName string
}

//Error implementacion de la interfaz error
func (e *ErrFailedToInsert) Error() string {
	return fmt.Sprintf("the model '%s' can not be inserted !", e.ModelName)
}

//ErrFailedToUpdate error retornado cuando no se pudo hacer la modificacion de la data solicitada en el modelo de forma correcta
type ErrFailedToUpdate struct {
	ModelName string
}

//Error implementacion de la interfaz error
func (e *ErrFailedToUpdate) Error() string {
	return fmt.Sprintf("the model '%s' can not be updated !", e.ModelName)
}

//ErrFailedToDelete error retornado cuando se ha producido un fallo en la eliminacion del modelo referenciado
type ErrFailedToDelete struct {
	ModelName string
}

//Error implementacion de la interfaz error
func (e *ErrFailedToDelete) Error() string {
	return fmt.Sprintf("the model '%s' can not be deleted !", e.ModelName)
}
