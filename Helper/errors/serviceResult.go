package errors

import (
	"fmt"
	"strings"
)

//ErrParamsNull ocurre cuando uno o mas parametros se han recibido vacios
type ErrParamsNull struct {
	Params []string
}

//Error implementacion de la interfaz error
func (e *ErrParamsNull) Error() string {
	makeString := func(nodes []string) string {
		if len(nodes) == 0 {
			return "''"
		}
		return "'" + strings.Join(nodes, "', '") + "'"
	}
	return fmt.Sprintf("the fields %s can not be empty!", makeString(e.Params))
}

//ErrSerialization ocurre cuando una vista u modelo no pudo ser serializado a un json en crudo (json.RawMessage)
type ErrSerialization struct {
	ViewName string
}

//Error implementacion de la interfaz error
func (e *ErrSerialization) Error() string {
	return fmt.Sprintf("the view '%s' can not be serialized !", e.ViewName)
}
