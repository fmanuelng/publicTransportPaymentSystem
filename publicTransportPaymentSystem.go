package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/labstack/echo/middleware"

	"github.com/labstack/echo"
	"gitlab.com/fmanuelng/publicTransportPaymentSystem/Routes"
)

//main funcion principal del aplicativo
func main() {
	e := echo.New()
	e.Logger.SetOutput(GetLoggerFile())
	e.Use(middleware.Recover(), middleware.Logger())
	Routes.InitRoutes(e)
	fmt.Print("runnig")
	e.Start(":8080")
}

//GetLoggerFile retorna un nuevo fichero donde va a ser almacenado los logs del  aplicativo
func GetLoggerFile() *os.File {
	var file *os.File
	dir, er := filepath.Abs(filepath.Dir(os.Args[0]))
	if er != nil {
		log.Fatal(er)
	}
	if _, err := os.Stat(filepath.Join(dir, "defaults", "logs")); os.IsNotExist(err) {
		os.MkdirAll(filepath.Join(dir, "defaults", "logs"), os.ModePerm)
		f, er := os.Create(filepath.Join(dir, "defaults", "logs", fmt.Sprintf(`log_%s`, time.Now().Format("2006-01-02")), fmt.Sprintf(`log_%s.txt`, time.Now().Format("2006-01-02T8:41PM"))))
		if er != nil {
			panic(er)
		}
		file = f
	}
	return file
}
